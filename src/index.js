import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import {
    HashRouter,
    Route,
    Switch
} from 'react-router-dom';
import App from './App';
import Login from './containers/Login';
import store from './store/store'
import registerServiceWorker from './registerServiceWorker';

import './assets/css/bootstrap.min.css';
import './assets/css/animate.min.css';
import './assets/sass/light-bootstrap-dashboard.css';
import './assets/css/demo.css';
import './assets/css/pe-icon-7-stroke.css';
import './index.css'

render(
  <Provider store = { store }>
    <HashRouter>
      <Switch>
        <Route path='/login' component={ Login } />
        <Route path="/" name="Home" component={App}/>
      </Switch>
    </HashRouter>
  </Provider>
  , document.getElementById('root'));
registerServiceWorker();
