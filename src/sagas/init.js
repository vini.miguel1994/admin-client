import { takeLatest, put, call, select } from 'redux-saga/effects'
import * as type from '../actions/types'

function apiLogin({email, password}) {
  return new Promise((resolve, reject) => {
    if ( email === 'pontal' && password === 'q1w2e3') {
      setTimeout(() => {
        resolve();
      }, 2000)
    } else {
      reject();
    }
  });
};

function* asyncLogin(action) {
  try {
    yield call(apiLogin, action.payload);

    yield put({ type: type.LOGIN })
    yield select(i => console.log(i))
  } catch (e) {
    yield put({ type: type.ERROR , payload: action.payload})
  }
};

function* asyncLogout() {
  yield put({ type: type.LOGOUT })
};

export default function* root() {
  yield [
    takeLatest('REQUEST_LOGIN', asyncLogin),
    takeLatest('ASYNC_LOGOUT', asyncLogout)
  ];
}
