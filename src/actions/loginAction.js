export function loginUser({ email, password}) {
    return {
      type: 'REQUEST_LOGIN',
      payload: {
        email,
        password
      }
    }

}

export function logoutUser() {
  return {
    type: 'ASYNC_LOGOUT'
  }
}
