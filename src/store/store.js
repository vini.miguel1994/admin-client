import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import appReducer from '../reducers/appReducers'
import rootSaga from '../sagas/init'

const sagaMiddleware = createSagaMiddleware()

export default createStore(combineReducers({
    user: appReducer
  }),
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga)
