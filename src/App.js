import React, { Component } from 'react';
import Home from './containers/Home';
import Sidebar from './components/Sidebar/Sidebar';
import Header from './components/Header/Header';

class App extends Component {
  render() {
    return (
      <div className="wrapper">
        <Sidebar />
        <div id="main-panel" className="main-panel">
          <Header />

        </div>
      </div>
    );
  }
}

export default App;
