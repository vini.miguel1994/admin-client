import React, { PureComponent } from 'react';
import { NavLink } from 'react-router-dom';

class Sidebar extends PureComponent {
  constructor(props) {
    super(props);
  }


  render(){
    return (
      <div id="sidebar" className="sidebar">
            <div className="logo">
              <a href="#" className="simple-text logo-normal">
                Admin
              </a>
            </div>
            <div className="sidebar-wrapper">
              <ul className="nav">
                <li className="active">
                  <NavLink to="/" className="nav-link" activeClassName="active">
                      <p>Dashboard</p>
                  </NavLink>
                </li>
              </ul>

            </div>
      </div>
    )
  }
}

export default Sidebar;
