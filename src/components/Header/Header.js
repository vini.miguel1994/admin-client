import React , { PureComponent } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class Header extends PureComponent {
  render() {
    return(
      <Navbar fluid>
        <Navbar.Header>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1} href="#">
    				Sms
    			</NavItem>
        </Nav>
      </Navbar>
    )
  }
}

export default Header;
