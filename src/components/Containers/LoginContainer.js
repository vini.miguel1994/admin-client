import styled from 'styled-components';

const LoginContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  > form {
    border: 1px solid grey;
    width: 300px;
  }
`;

export default LoginContainer;
