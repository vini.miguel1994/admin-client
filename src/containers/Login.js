import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NavLink, withRouter } from 'react-router-dom';

import LoginContainer from '../components/Containers/LoginContainer';
import * as loginActions from '../actions/loginAction';

class Login extends Component {
    loginUser = async (e) => {
      let user = {
        email : 'pontal',
        password : 'q1w2e3'
      }
        e.preventDefault();
      await this.props.actions.loginUser(user)
        // await this.props.actions.loginUser(email, password);
    };

    handleChangeEmail = (e) => {
        this.setState({
            email: e.target.value
        })
    }
    handleChangePass = (e) => {
        this.setState({
            password: e.target.value
        })
    }

    render() {
        const { user } = this.props;
        return(
          <LoginContainer>
            <form className="form" method="" action="">
                <div className="header header-primary text-center">
                </div>
                <div className="input-group input-lg">
                    <span className="input-group-addon">
                        <i className="pe-7s-id"></i>
                    </span>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Email"
                        onChange={this.handleChangeEmail}
                    />
                </div>
                <div className="input-group input-lg">
                    <span className="input-group-addon">
                        <i className="pe-7s-key"></i>
                    </span>
                    <input
                        type="password"
                        placeholder="Senha"
                        className="form-control"
                        onChange={this.handleChangePass}
                    />
                </div>
                <div className="text-center form-group">
                    <button
                        className="btn btn-info btn-block btn-round"
                        onClick={this.loginUser}
                        >
                        Login
                    </button>
                </div>
            </form>
          </LoginContainer>
        );
    }
}


function mapStateToProps(state) {
    const { user } = state;
    return { user }
}

function mapDispatchToProps(dispatch) {
    return {
      actions: bindActionCreators(loginActions, dispatch),
    }
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
