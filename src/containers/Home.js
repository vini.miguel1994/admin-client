import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as loginActions from '../actions/loginAction'

class Home extends Component {
  loginUser = () => {
    let user = {
      email : 'pontal',
      password : 'q1w2e3'
    }
    this.props.actions.loginUser(user)
  }
  logoutUser = () => {
    this.props.actions.logoutUser()
  }

  render() {
    const {user} = this.props;
    return (
      <div>
        <h1>Só loga</h1>
        <button onClick={this.loginUser} >Logar</button>
        <button onClick={this.logoutUser} >Logout</button>
        {user.isLoading && <p>Carregando...</p>}
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { user } = state;
  console.log(user)
  return {
    user
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(loginActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
