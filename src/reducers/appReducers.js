import * as type from '../actions/types';
const initialState = {
  isAuth: false,
  isError: false,
  isLoading: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case type.REQUEST:
      return { ...state, isLoading: true }
    case type.LOGIN:
      return { ...state, isAuth: true, isLoading: false };
    case type.LOGOUT:
      return { ...state, isAuth: false };
    case type.ERROR:
      return {...state, isAuth: false, isError: action.payload };
    default:
      return state;
  }
}
